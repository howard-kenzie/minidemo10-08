const container = document.getElementById("root")

const random_phrases = [
    "Sim",
    "Não",
    "Acho que sim",
    "Acho que não",
    "Você deveria pensar melhor",
    "Nem que sim nem que nao, muito pelo contrario"
]

const  startScreen = () => {
    const startButton = document.createElement("button")
    startButton.innerHTML = "Iniciar"
    startButton.classList.add("startButton")
    startButton.id = "startButton"
    container.appendChild(startButton)
}

startScreen()

const  startGame = () => {
    const startButton = document.getElementById("startButton");
    startButton.addEventListener("click", () => {
        container.innerHTML = "";
        const ball = document.createElement("div")
        ball.classList.add("ball")
        const output = document.createElement("div")
        output.id = "output"
        output.classList.add("ball__output")
        output.innerHTML = "8"
        output.style.fontSize = "100px"
        ball.appendChild(output)
        container.appendChild(ball)
        showResponse()
    })
}

startGame()

const randomPhrase = () => {
    const max = random_phrases.length
    return Math.floor(Math.random() * max);
}

const  showResponse = () => {
    const output = document.getElementById("output")
    console.log(output)
    output.addEventListener("click", () => {
        output.innerHTML = ""
        output.style.fontSize = "1rem"
        let index = randomPhrase()
        let phrase = random_phrases[index]
        output.innerHTML = phrase
    })
}
